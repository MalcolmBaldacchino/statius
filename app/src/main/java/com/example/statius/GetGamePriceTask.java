package com.example.statius;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

public class GetGamePriceTask extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... gameID){
        String price = "";

        Functions functions = new Functions();
        String JSONResultEUR = functions.GetGamePriceEUR(gameID[0]);

        JSONObject jsonEUR = null;

        try{
            jsonEUR = new JSONObject(JSONResultEUR);
            JSONObject gameEUR = jsonEUR.getJSONObject(gameID[0]);
            JSONObject dataEUR = gameEUR.getJSONObject("data");
            JSONObject overviewEUR = dataEUR.getJSONObject("price_overview");
            price = overviewEUR.getString("final_formatted");

        } catch (JSONException E) {
            E.printStackTrace();
        }

        return price;
    }
}
