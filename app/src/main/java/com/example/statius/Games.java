package com.example.statius;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class Games extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {

    private SharedPreferences myPreferences;
    private SharedPreferences.Editor myEditor;

    private EditText gameID;
    private TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_games);

        gameID = findViewById(R.id.etxt_GameID);
        price = findViewById(R.id.txtv_priceEUR);

        myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        myEditor = myPreferences.edit();
    }

    protected void onPause(){
        super.onPause();

        //GameID
        String myGameID = gameID.getText().toString();
        myEditor.putString(getString(R.string.gameID), myGameID);
        myEditor.commit();

        //Price
        String myPrice = price.getText().toString();
        myEditor.putString(getString(R.string.price), myPrice);
        myEditor.commit();

        //Date&Time
        String currentTime = Calendar.getInstance().getTime().toString();
        myEditor.putString(getString(R.string.gamesTime), currentTime);
        myEditor.commit();
    }

    protected void onStop(){
        super.onStop();

        //GameID
        String myGameID = gameID.getText().toString();
        myEditor.putString(getString(R.string.gameID), myGameID);
        myEditor.commit();

        //Price
        String myPrice = price.getText().toString();
        myEditor.putString(getString(R.string.price), myPrice);
        myEditor.commit();

        //Date&Time
        String currentTime = Calendar.getInstance().getTime().toString();
        myEditor.putString(getString(R.string.gamesTime), currentTime);
        myEditor.commit();
    }

    protected void OnResume(){
        super.onResume();

        //GameID
        String mySteamID = myPreferences.getString(getString(R.string.gameID), "");
        gameID.setText(mySteamID);

        //Price
        String myPrice = myPreferences.getString(getString(R.string.price), "");
        price.setText(myPrice);

        //Date&Time
        String prevTime = myPreferences.getString(getString(R.string.gamesTime),"");
        Snackbar.make(this.findViewById(android.R.id.content), prevTime, 5000).show();
    }

    protected void onStart(){
        super.onStart();

        //GameID
        String mySteamID = myPreferences.getString(getString(R.string.gameID), "");
        gameID.setText(mySteamID);

        //Price
        String myPrice = myPreferences.getString(getString(R.string.price), "");
        price.setText(myPrice);

        //Date&Time
        String prevTime = myPreferences.getString(getString(R.string.gamesTime),"");
        Snackbar.make(this.findViewById(android.R.id.content), prevTime, 5000).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            Intent i = new Intent(this,Fingerprint.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);

        } else if (id == R.id.nav_profile) {
            Intent i = new Intent(this,Profile.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);

        } else if (id == R.id.nav_games) {
            Intent i = new Intent(this,Games.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Game IDs:
    // Rainbow 6: 359550
    // Civ V: 8930
    // Divinity Original Sin 2: 435150

    public void getGamePrice(View view){
        EditText editText = findViewById(R.id.etxt_GameID);
        GetGamePriceTask ggpt = new GetGamePriceTask();

        try{
            String price = ggpt.execute(editText.getText().toString()).get();

            TextView txtEUR = findViewById(R.id.txtv_priceEUR);
            txtEUR.setText(price);

        } catch (ExecutionException E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }
    }

}
