package com.example.statius;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

public class GetPlayerGamesTask extends AsyncTask<String, Void, Integer> {

    @Override
    protected Integer doInBackground(String... steamID){
        int games = 0;

        Functions functions = new Functions();
        String JSONResult = functions.GetPlayerGames(steamID[0]);

        JSONObject json = null;

        try{
            json = new JSONObject(JSONResult);
            JSONObject responseObj = json.getJSONObject("response");
            games = responseObj.getInt("game_count");

        } catch (JSONException E) {
            E.printStackTrace();
        }

        return games;
    }
}
