package com.example.statius;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class Profile extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private SharedPreferences myPreferences;
    private SharedPreferences.Editor myEditor;

    private EditText steamID;
    private TextView profileURL, name, realName, countryCode, level, games, game1, game2, game3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_profile);

        steamID = findViewById(R.id.etxt_SteamID);
        profileURL = findViewById(R.id.txtv_profileURL);
        name = findViewById(R.id.txtv_name);
        realName = findViewById(R.id.txtv_realName);
        countryCode = findViewById(R.id.txtv_countryCode);
        level = findViewById(R.id.txtv_level);
        games = findViewById(R.id.txtv_gamesOwned);
        game1 = findViewById(R.id.txtv_game1);
        game2 = findViewById(R.id.txtv_game2);
        game3 = findViewById(R.id.txtv_game3);

        myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        myEditor = myPreferences.edit();
    }

    protected void onPause(){
        super.onPause();

        //SteamID
        String mySteamID = steamID.getText().toString();
        myEditor.putString(getString(R.string.steamID), mySteamID);
        myEditor.commit();

        //ProfileURL
        String myProfileURL = profileURL.getText().toString();
        myEditor.putString(getString(R.string.profileURL), myProfileURL);
        myEditor.commit();

        //Name
        String myName = name.getText().toString();
        myEditor.putString(getString(R.string.name), myName);
        myEditor.commit();

        //RealName
        String myRealName = realName.getText().toString();
        myEditor.putString(getString(R.string.realName), myRealName);
        myEditor.commit();

        //CountryCode
        String myCountryCode = countryCode.getText().toString();
        myEditor.putString(getString(R.string.countryCode), myCountryCode);
        myEditor.commit();

        //Level
        String myLevel = level.getText().toString();
        myEditor.putString(getString(R.string.level), myLevel);
        myEditor.commit();

        //GamesOwned
        String myGames = games.getText().toString();
        myEditor.putString(getString(R.string.games), myGames);
        myEditor.commit();

        //Game1
        String myGame1 = game1.getText().toString();
        myEditor.putString(getString(R.string.game1), myGame1);
        myEditor.commit();

        //Game2
        String myGame2 = game2.getText().toString();
        myEditor.putString(getString(R.string.game2), myGame2);
        myEditor.commit();

        //Game3
        String myGame3 = game3.getText().toString();
        myEditor.putString(getString(R.string.game3), myGame3);
        myEditor.commit();

        //Date&Time
        String currentTime = Calendar.getInstance().getTime().toString();
        myEditor.putString(getString(R.string.profileTime), currentTime);
        myEditor.commit();
    }

    protected void onStop(){
        super.onStop();

        //SteamID
        String mySteamID = steamID.getText().toString();
        myEditor.putString(getString(R.string.steamID), mySteamID);
        myEditor.commit();

        //ProfileURL
        String myProfileURL = profileURL.getText().toString();
        myEditor.putString(getString(R.string.profileURL), myProfileURL);
        myEditor.commit();

        //Name
        String myName = name.getText().toString();
        myEditor.putString(getString(R.string.name), myName);
        myEditor.commit();

        //RealName
        String myRealName = realName.getText().toString();
        myEditor.putString(getString(R.string.realName), myRealName);
        myEditor.commit();

        //CountryCode
        String myCountryCode = countryCode.getText().toString();
        myEditor.putString(getString(R.string.countryCode), myCountryCode);
        myEditor.commit();

        //Level
        String myLevel = level.getText().toString();
        myEditor.putString(getString(R.string.level), myLevel);
        myEditor.commit();

        //GamesOwned
        String myGames = games.getText().toString();
        myEditor.putString(getString(R.string.games), myGames);
        myEditor.commit();

        //Game1
        String myGame1 = game1.getText().toString();
        myEditor.putString(getString(R.string.game1), myGame1);
        myEditor.commit();

        //Game2
        String myGame2 = game2.getText().toString();
        myEditor.putString(getString(R.string.game2), myGame2);
        myEditor.commit();

        //Game3
        String myGame3 = game3.getText().toString();
        myEditor.putString(getString(R.string.game3), myGame3);
        myEditor.commit();

        //Date&Time
        String currentTime = Calendar.getInstance().getTime().toString();
        myEditor.putString(getString(R.string.profileTime), currentTime);
        myEditor.commit();
    }

    protected void OnResume(){
        super.onResume();

        //SteamID
        String mySteamID = myPreferences.getString(getString(R.string.steamID), "");
        steamID.setText(mySteamID);

        //ProfileURL
        String myProfileURL = myPreferences.getString(getString(R.string.profileURL),"");
        profileURL.setText(myProfileURL);

        //Name
        String myName = myPreferences.getString(getString(R.string.name), "");
        name.setText(myName);

        //RealName
        String myRealName = myPreferences.getString(getString(R.string.realName), "");
        realName.setText(myRealName);

        //CountryCode
        String myCountryCode = myPreferences.getString(getString(R.string.countryCode),"");
        countryCode.setText(myCountryCode);

        //Level
        String myLevel = myPreferences.getString(getString(R.string.level),"");
        level.setText(myLevel);

        //GamesOwned
        String myGames = myPreferences.getString(getString(R.string.games),"");
        games.setText(myGames);

        //Game1
        String myGame1 = myPreferences.getString(getString(R.string.game1),"");
        game1.setText(myGame1);

        //Game2
        String myGame2 = myPreferences.getString(getString(R.string.game2),"");
        game2.setText(myGame2);

        //Game3
        String myGame3 = myPreferences.getString(getString(R.string.game3),"");
        game3.setText(myGame3);

        //Date&Time
        String prevTime = myPreferences.getString(getString(R.string.profileTime),"");
        Snackbar.make(this.findViewById(android.R.id.content), prevTime, 5000).show();
    }

    protected void onStart(){
        super.onStart();

        //SteamID
        String mySteamID = myPreferences.getString(getString(R.string.steamID), "");
        steamID.setText(mySteamID);

        //ProfileURL
        String myProfileURL = myPreferences.getString(getString(R.string.profileURL),"");
        profileURL.setText(myProfileURL);

        //Name
        String myName = myPreferences.getString(getString(R.string.name), "");
        name.setText(myName);

        //RealName
        String myRealName = myPreferences.getString(getString(R.string.realName), "");
        realName.setText(myRealName);

        //CountryCode
        String myCountryCode = myPreferences.getString(getString(R.string.countryCode),"");
        countryCode.setText(myCountryCode);

        //Level
        String myLevel = myPreferences.getString(getString(R.string.level),"");
        level.setText(myLevel);

        //GamesOwned
        String myGames = myPreferences.getString(getString(R.string.games),"");
        games.setText(myGames);

        //Game1
        String myGame1 = myPreferences.getString(getString(R.string.game1),"");
        game1.setText(myGame1);

        //Game2
        String myGame2 = myPreferences.getString(getString(R.string.game2),"");
        game2.setText(myGame2);

        //Game3
        String myGame3 = myPreferences.getString(getString(R.string.game3),"");
        game3.setText(myGame3);

        //Date&Time
        String prevTime = myPreferences.getString(getString(R.string.profileTime),"");
        Snackbar.make(this.findViewById(android.R.id.content), prevTime, 5000).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            Intent i = new Intent(this,Fingerprint.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);

        } else if (id == R.id.nav_profile) {
            Intent i = new Intent(this,Profile.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);

        } else if (id == R.id.nav_games) {
            Intent i = new Intent(this,Games.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(i);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getPlayerInfo(View view){
        EditText editText = findViewById(R.id.etxt_SteamID);
        GetPlayerInfoTask gpit = new GetPlayerInfoTask();

        try{
            String[] array = gpit.execute(editText.getText().toString()).get();

            TextView txtProfileURL = findViewById(R.id.txtv_profileURL);
            txtProfileURL.setText(array[0]);

            TextView txtName = findViewById(R.id.txtv_name);
            txtName.setText(array[1]);

            TextView txtRealName = findViewById(R.id.txtv_realName);
            txtRealName.setText(array[2]);

            TextView txtCountry = findViewById(R.id.txtv_countryCode);
            txtCountry.setText(array[3]);

        } catch (ExecutionException E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }
    }

    public void getPlayerLevel(View view){
        EditText editText = findViewById(R.id.etxt_SteamID);
        GetPlayerLevelTask gplt = new GetPlayerLevelTask();

        try{
            int level = gplt.execute(editText.getText().toString()).get();

            TextView txtLevel = findViewById(R.id.txtv_level);
            txtLevel.setText(String.valueOf(level));

        } catch (ExecutionException E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }
    }

    public void getPlayerGames(View view){
        EditText editText = findViewById(R.id.etxt_SteamID);
        GetPlayerGamesTask gppt = new GetPlayerGamesTask();

        try{
            int games = gppt.execute(editText.getText().toString()).get();

            TextView txtGames = findViewById(R.id.txtv_gamesOwned);
            txtGames.setText(String.valueOf(games));

        } catch (ExecutionException E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }
    }

    public void getRecentPlayedGames(View view){
        EditText editText = findViewById(R.id.etxt_SteamID);
        GetRecentPlayedGamesTask grpgt = new GetRecentPlayedGamesTask();

        try{
            String[] array = grpgt.execute(editText.getText().toString()).get();

            TextView txtGame1 = findViewById(R.id.txtv_game1);
            txtGame1.setText(array[0]);

            TextView txtGame2 = findViewById(R.id.txtv_game2);
            txtGame2.setText(array[1]);

            TextView txtGame3 = findViewById(R.id.txtv_game3);
            txtGame3.setText(array[2]);

        } catch (ExecutionException E) {
            E.printStackTrace();
        } catch (InterruptedException E) {
            E.printStackTrace();
        }
    }

    //Steam IDs:
    //76561197960435530 //Do not use!
    //76561197968423451
    //76561197984432884
    //76561198071621154

    public void getProfile(View view){
        getPlayerLevel(view);
        getPlayerGames(view);
        getPlayerInfo(view);
        getRecentPlayedGames(view);
    }
}
