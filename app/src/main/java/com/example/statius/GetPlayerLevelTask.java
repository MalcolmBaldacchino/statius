package com.example.statius;

import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;

public class GetPlayerLevelTask extends AsyncTask<String, Void, Integer> {

    @Override
    protected Integer doInBackground(String... steamID){
        int level = 0;

        Functions functions = new Functions();
        String JSONResult = functions.GetPlayerLevel(steamID[0]);

        JSONObject json = null;

        try{
            json = new JSONObject(JSONResult);
            JSONObject responseObj = json.getJSONObject("response");
            level = responseObj.getInt("player_level");

        } catch (JSONException E) {
            E.printStackTrace();
        }

        return level;
    }
}
