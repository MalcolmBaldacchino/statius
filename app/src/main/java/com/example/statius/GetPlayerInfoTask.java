package com.example.statius;

import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetPlayerInfoTask extends AsyncTask<String, Void, String[]> {

    @Override
    protected String[] doInBackground(String... steamID){
        String array[] = new String[4];

        Functions functions = new Functions();
        String JSONResult = functions.GetPlayerInfo(steamID[0]);

        JSONObject json = null;

        try{
            json = new JSONObject(JSONResult);
            JSONObject responseObj = json.getJSONObject("response");
            JSONArray playersArr = responseObj.getJSONArray("players");
            JSONObject playersObj = playersArr.getJSONObject(0);
            array[0] = playersObj.getString("profileurl");
            array[1] = playersObj.getString("personaname");
            array[2] = playersObj.getString("realname");
            array[3] = playersObj.getString("loccountrycode");

        } catch (JSONException E) {
            E.printStackTrace();
        }

        return array;
    }
}