package com.example.statius;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class Functions {
    private static String Base_URL = "https://api.steampowered.com/";
    private static String API_KEY = "E089A8F142C410A9EEE3121889C1995A";

    public String GetPlayerInfo(String steamID){
        HttpsURLConnection connection = null;
        InputStream stream = null;

        try{
            String link = Base_URL + "ISteamUser/GetPlayerSummaries/v0002/?key=" + API_KEY + "&steamids=" + steamID;

            connection = (HttpsURLConnection) (new URL(link)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            StringBuffer buffer = new StringBuffer();
            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while((line = reader.readLine()) != null)
                buffer.append(line + "\r\n");
            stream.close();
            connection.disconnect();

            return buffer.toString();

        } catch (Throwable T){
            T.printStackTrace();

        } finally {
            try { stream.close(); } catch(Throwable T) {}
            try { connection.disconnect(); } catch(Throwable T) {}
        }

        return null;
    }

    public String GetPlayerLevel(String steamID){
        HttpsURLConnection connection = null;
        InputStream stream = null;

        try{
            String link = Base_URL + "IPlayerService/GetSteamLevel/v1/?key=" + API_KEY + "&steamid=" + steamID;

            connection = (HttpsURLConnection) (new URL(link)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            StringBuffer buffer = new StringBuffer();
            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while((line = reader.readLine()) != null)
                buffer.append(line + "\r\n");
            stream.close();
            connection.disconnect();

            return buffer.toString();

        } catch (Throwable T){
            T.printStackTrace();

        } finally {
            try { stream.close(); } catch(Throwable T) {}
            try { connection.disconnect(); } catch(Throwable T) {}
        }

        return null;
    }

    public String GetPlayerGames(String steamID){
        HttpsURLConnection connection = null;
        InputStream stream = null;

        try{
            String link = Base_URL + "IPlayerService/GetOwnedGames/v0001/?key=" + API_KEY + "&steamid=" + steamID;

            connection = (HttpsURLConnection) (new URL(link)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            StringBuffer buffer = new StringBuffer();
            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while((line = reader.readLine()) != null)
                buffer.append(line + "\r\n");
            stream.close();
            connection.disconnect();

            return buffer.toString();

        } catch (Throwable T){
            T.printStackTrace();

        } finally {
            try { stream.close(); } catch(Throwable T) {}
            try { connection.disconnect(); } catch(Throwable T) {}
        }

        return null;
    }

    public String GetRecentlyPlayedGames (String steamID){
        HttpsURLConnection connection = null;
        InputStream stream = null;

        try{
            String link = Base_URL + "IPlayerService/GetRecentlyPlayedGames/v0001/?key=" + API_KEY + "&steamid=" + steamID;

            connection = (HttpsURLConnection) (new URL(link)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            StringBuffer buffer = new StringBuffer();
            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while((line = reader.readLine()) != null)
                buffer.append(line + "\r\n");
            stream.close();
            connection.disconnect();

            return buffer.toString();

        } catch (Throwable T){
            T.printStackTrace();

        } finally {
            try { stream.close(); } catch(Throwable T) {}
            try { connection.disconnect(); } catch(Throwable T) {}
        }

        return null;
    }

    public String GetGamePriceEUR(String gameID){
        HttpsURLConnection connection = null;
        InputStream stream = null;

        try{
            String link = "https://store.steampowered.com/api/appdetails?appids=" + gameID + "&filters=price_overview&cc=";

            connection = (HttpsURLConnection) (new URL(link)).openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.setDoOutput(false);
            connection.connect();

            StringBuffer buffer = new StringBuffer();
            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            String line = null;
            while((line = reader.readLine()) != null)
                buffer.append(line + "\r\n");
            stream.close();
            connection.disconnect();

            return buffer.toString();

        } catch (Throwable T){
            T.printStackTrace();

        } finally {
            try { stream.close(); } catch(Throwable T) {}
            try { connection.disconnect(); } catch(Throwable T) {}
        }

        return null;
    }
}
