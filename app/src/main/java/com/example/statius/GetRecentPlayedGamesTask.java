package com.example.statius;

import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetRecentPlayedGamesTask extends AsyncTask<String, Void, String[]> {

    @Override
    protected String[] doInBackground(String... steamID){
        String array[] = new String[3];

        Functions functions = new Functions();
        String JSONResult = functions.GetRecentlyPlayedGames(steamID[0]);

        JSONObject json = null;

        try{
            json = new JSONObject(JSONResult);
            JSONObject responseObj = json.getJSONObject("response");
            JSONArray playersArr = responseObj.getJSONArray("games");
            JSONObject gameObj1 = playersArr.getJSONObject(0);
            JSONObject gameObj2 = playersArr.getJSONObject(1);
            JSONObject gameObj3 = playersArr.getJSONObject(2);
            array[0] = gameObj1.getString("name");
            array[1] = gameObj2.getString("name");
            array[2] = gameObj3.getString("name");

        } catch (JSONException E) {
            E.printStackTrace();
        }

        return array;
    }
}
